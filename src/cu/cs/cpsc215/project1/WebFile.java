package cu.cs.cpsc215.project1;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;

import java.io.IOException;


public class WebFile implements WebElement{
	private String absoluteURL;
	private String name;
	private int depth;
	private DownloadRepository down;
	
	public WebFile(String address, int idepth, DownloadRepository dwn)  {
		down = dwn;
		depth = idepth;
		absoluteURL = address;
		
		//getName
		int indexofname = address.lastIndexOf("/");
		if (indexofname == address.length()) {
			address = address.substring(1, indexofname);
		}
		//indexofname = address.lastIndexOf("/");
		name = address.substring(indexofname, address.length());
		System.out.println("		"+name);
	}
	
	public void saveToDisk() throws MalformedURLException,IOException{
		URL url = new URL(absoluteURL);
		down.save(url,depth,name);
	}
}
