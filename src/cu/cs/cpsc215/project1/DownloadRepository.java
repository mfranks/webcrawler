package cu.cs.cpsc215.project1;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;

public class DownloadRepository {
	
	private static DownloadRepository instance = null;
	private static String rootFolder;
	private int i;
	
	protected DownloadRepository(String folder) {
	 rootFolder = folder;
	}
	
	public static DownloadRepository getInstance() {
		if(instance == null) {
			instance = new DownloadRepository(rootFolder);
		}
		return instance;
	}
	
	public boolean isNameUnique(String name) {		
		File dir = new File(".");
		File[] filesList = dir.listFiles();
		for (File file : filesList) {
		    if (file.isFile()) {
		        if(name.equals(file.getName())){
		        	i++;
		        	return false;
		        }
		    }
		}
		return true;
	}
	
	public boolean save(URL absoluteURL,int depth,String name) {
		InputStream in = null;
		try {
			in = absoluteURL.openStream();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		OutputStream out = null;
		
		if(!isNameUnique(name)) {
			name+=depth+=i;
		}
		try {
			out = new BufferedOutputStream(new FileOutputStream(rootFolder + name));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		try {
			for (int b; (b = in.read()) != -1;) {
				out.write(b);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			out.close();
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return true;
	}
}
