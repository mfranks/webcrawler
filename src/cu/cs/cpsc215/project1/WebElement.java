package cu.cs.cpsc215.project1;

import java.io.IOException;
import java.net.MalformedURLException;

public interface WebElement {
	public void saveToDisk() throws MalformedURLException, IOException;
}
