package cu.cs.cpsc215.project1;

import java.io.*;
import java.net.*;

public class WebCrawler {
	private static String seedString;
	private static int depth;
	private static String rootFolder;
	private static DownloadRepository dwn;
	
	//public static void main(String i_url, int i_depth, String i_destination) throws IOException { 
	public static void main(String args[]){ 
		
		seedString = args[0];
		depth = 2;//args[1];
		rootFolder = args[2];
		dwn = new DownloadRepository(rootFolder);
		WebPage seed = new WebPage(seedString,depth,dwn);
		seed.crawl();
	}
}
