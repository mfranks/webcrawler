package cu.cs.cpsc215.project1;

import java.io.BufferedReader;


import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class WebPage implements WebElement {
	
	private String url;
	private int depth;
	private Document page;
	Elements images, files, pages; 
	WebPage[] p;
	DownloadRepository down;

	public WebPage(String newurl, int degree, DownloadRepository dwn) {
		url = newurl;
		depth = degree;
		down = dwn;
		
		System.out.println("		"+url);
		
	}
	
	public void crawl(){
		try {
			page = Jsoup.connect(url).get();
		
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		System.out.println(depth + "  "+ url);
		
		images = join(images, getImages());
		files = join(files, getFiles());
		pages = join(pages, getWebPages());
	
		if(depth>0) {
			depth--;
			for(Element el : pages) {
				if(!el.absUrl("href").equals(url)) {
					WebPage newpage = new WebPage(el.absUrl("href"),depth,down);
					newpage.crawl();
				}
			}
		}

	}

	public Elements join(Elements a, Elements b) {
		Elements c = a;
		for(Element src : b) {
			c.add(src);
		}
		return c;
	}
	
	
	public Elements getImages() {
	    images = page.select("img[src]");
	    System.out.println("	Images: "+images.size());

	    
	    for(Element el : images) {
	    	el.absUrl("src");
	    	WebImage image = new WebImage(el.absUrl("src"), depth, down);
	    	
	    	try {
				image.saveToDisk();
			} 
	    	catch (MalformedURLException e) {
				e.printStackTrace();
			}
	    	catch (IOException e) {
	    		e.printStackTrace();
	    	}
	    }
        return images;
	}
	
	public Elements getFiles() {
		
		files = page.select("link");
		System.out.println("	Files: "+files.size());
		
	    for(Element el : files) {
	    	WebFile image = new WebFile(el.absUrl("href"), depth, down);
	    	try {
				image.saveToDisk();
			} 
	    	catch (MalformedURLException e) {
				e.printStackTrace();
			}
	    	catch (IOException e) {
	    		e.printStackTrace();
	    	}
	    }
        return files;
	}
	
	public Elements getWebPages() {
		pages = page.select("a[href]");
		System.out.println("	Links: "+ pages.size());
		p = new WebPage[pages.size()];
		for(Element el : pages) {
			WebPage page = new WebPage(el.attr("abs:href"), depth, down);
	    	try {
				page.saveToDisk();
			}
	    	catch (MalformedURLException e) {
				e.printStackTrace();
			}
	    	catch (IOException e) {
	    		e.printStackTrace();
	    	}
	    }
		return pages;
		
	}
	public void saveToDisk() throws MalformedURLException, IOException{
		
	}
}
